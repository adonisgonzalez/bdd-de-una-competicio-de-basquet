# RESUMEN DE REQUERIMIENTO DE BASE DE DATOS #

En el problema que s’ha proposat te la finalitat de dissenyar i gestionar un
sistema de bases de dades de una competició de basquet, tenint en compte
factors com per exemple l’edat del jugadors, competició a la que pertanyen,
camps entre altres aspectes.
Per satisfer les necessitats de informació requerides es dissenyarà un model
Entitat-Relació. Per garantir bon funcionament del SBD(sistema de bases de
dades) s’ha de realitzar un sistema consistent evitant valors incorrectes i
informació repetida, el qual es oferirà un rendiment més òptim.