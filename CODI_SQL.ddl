-- Generado por Oracle SQL Developer Data Modeler 4.1.5.907
--   en:        2017-01-15 22:47:12 CET
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g




DROP TABLE Acta CASCADE CONSTRAINTS ;

DROP TABLE Arbitres CASCADE CONSTRAINTS ;

DROP TABLE Camps CASCADE CONSTRAINTS ;

DROP TABLE Categories CASCADE CONSTRAINTS ;

DROP TABLE Equip CASCADE CONSTRAINTS ;

DROP TABLE Equips_Rivals CASCADE CONSTRAINTS ;

DROP TABLE Jugadors CASCADE CONSTRAINTS ;

DROP TABLE Partit CASCADE CONSTRAINTS ;

DROP TABLE Persona CASCADE CONSTRAINTS ;

DROP TABLE Staff CASCADE CONSTRAINTS ;

DROP TABLE Titulacio CASCADE CONSTRAINTS ;

DROP TABLE Treballan CASCADE CONSTRAINTS ;

DROP TABLE alternativa_pluja CASCADE CONSTRAINTS ;

DROP TABLE creacio_acta CASCADE CONSTRAINTS ;

DROP TABLE equip_entrena_camp CASCADE CONSTRAINTS ;

CREATE TABLE Acta
  (
    Dorsal  VARCHAR2 (3) NOT NULL ,
    Quart   INTEGER NOT NULL ,
    Punts   INTEGER ,
    Faltes  INTEGER ,
    Tir_1   INTEGER ,
    Tir_2   INTEGER ,
    Tir_3   INTEGER ,
    Dorsal1 VARCHAR2 (3) ,
    Quart1  INTEGER
  ) ;
ALTER TABLE Acta ADD CONSTRAINT Acta_PK PRIMARY KEY ( Dorsal, Quart ) ;


CREATE TABLE Arbitres
  (
    Llicencia    VARCHAR2 (7) NOT NULL ,
    Nom          VARCHAR2 (20) ,
    Cognoms      VARCHAR2 (30) ,
    Colegi       VARCHAR2 (50) ,
    Mobil        VARCHAR2 (9) ,
    E_Mail       VARCHAR2 (40) ,
    Partit_Camp1 VARCHAR2 (50) ,
    Partit_Data1 DATE ,
    Partit_Camp  VARCHAR2 (50) ,
    Partit_Data  DATE
  ) ;


CREATE TABLE Camps
  (
    Nom       VARCHAR2 (50) NOT NULL ,
    Adre�a    VARCHAR2 (50) ,
    Graderia  CHAR (1) ,
    Capacitat INTEGER ,
    Cobert    CHAR (1)
  ) ;
ALTER TABLE Camps ADD CONSTRAINT Camps_PK PRIMARY KEY ( Nom ) ;


CREATE TABLE Categories
  (
    Nom        VARCHAR2 (20) NOT NULL ,
    Edat       VARCHAR2 (15) ,
    Cistella   VARCHAR2 (10) ,
    Pilota     VARCHAR2 (10) ,
    Equip_Nom  VARCHAR2 (50) NOT NULL ,
    Equip_Sexe VARCHAR2 (50) NOT NULL
  ) ;
ALTER TABLE Categories ADD CONSTRAINT Categories_PK PRIMARY KEY ( Nom ) ;


CREATE TABLE Equip
  (
    Nom             VARCHAR2 (50) NOT NULL ,
    Temporada       VARCHAR2 (50) ,
    Sexe            VARCHAR2 (50) NOT NULL ,
    Jugadors_Dorsal INTEGER NOT NULL ,
    Partit_Camp     VARCHAR2 (50) ,
    Partit_Data     DATE ,
    Partit_Camp1    VARCHAR2 (50) ,
    Partit_Data1    DATE ,
    Staff_Llicencia VARCHAR2 (7) NOT NULL
  ) ;
ALTER TABLE Equip ADD CONSTRAINT Equip_PK PRIMARY KEY ( Nom, Sexe ) ;


CREATE TABLE Equips_Rivals
  (
    Nom          VARCHAR2 (50) NOT NULL ,
    Categoria    VARCHAR2 (50) ,
    Temporada    VARCHAR2 (50) ,
    Sexe         VARCHAR2 (50) ,
    Adre�a       VARCHAR2 (50) ,
    Poblacio     VARCHAR2 (50) ,
    Any_Fundacio DATE ,
    Entrenador   VARCHAR2 (50) ,
    Mobil        VARCHAR2 (9) ,
    E_Mail       VARCHAR2 (50) ,
    Partit_Camp  VARCHAR2 (50) NOT NULL ,
    Partit_Data  DATE NOT NULL
  ) ;
ALTER TABLE Equips_Rivals ADD CONSTRAINT Equips_Rivals_PK PRIMARY KEY ( Nom ) ;


CREATE TABLE Jugadors
  (
    Dorsal         INTEGER NOT NULL ,
    Llicencia      VARCHAR2 (7) NOT NULL ,
    Nom            VARCHAR2 (15) ,
    Cognoms        VARCHAR2 (30) ,
    Adre�a         VARCHAR2 (30) ,
    Poblacio       VARCHAR2 (15) ,
    Data_Neixement DATE ,
    Procedencia    VARCHAR2 (50) ,
    Marxa          VARCHAR2 (50)
  ) ;
ALTER TABLE Jugadors ADD CONSTRAINT Jugadors_PK PRIMARY KEY ( Dorsal ) ;


CREATE TABLE Partit
  (
    Data        DATE NOT NULL ,
    Temporada   VARCHAR2 (7) ,
    Categoria   VARCHAR2 (20) ,
    Fase        VARCHAR2 (20) ,
    Grup        CHAR (1) ,
    Subgrup     INTEGER ,
    Rival       VARCHAR2 (20) ,
    Camp        VARCHAR2 (50) NOT NULL ,
    T_Propi     INTEGER ,
    T_Rival     INTEGER ,
    Jornada     INTEGER ,
    ID_Partit   VARCHAR2 (50) ,
    Fase1       VARCHAR2 (50) ,
    Puntuacio   INTEGER ,
    Acta_Dorsal VARCHAR2 (3) ,
    Acta_Quart  INTEGER
  ) ;
ALTER TABLE Partit ADD CONSTRAINT Partit_PK PRIMARY KEY ( Camp, Data ) ;


CREATE TABLE Persona
  (
    Nom           VARCHAR2 (50) ,
    Cognoms       VARCHAR2 (50) ,
    Licencia      VARCHAR2 (50) NOT NULL ,
    Adre�a        VARCHAR2 (50) ,
    Poblacio      VARCHAR2 (50) ,
    Data_Naxement DATE
  ) ;
ALTER TABLE Persona ADD CONSTRAINT Persona_PK PRIMARY KEY ( Licencia ) ;


CREATE TABLE Staff
  (
    Llicencia      VARCHAR2 (7) NOT NULL ,
    Nom            VARCHAR2 (20) ,
    Cognom         VARCHAR2 (30) ,
    Adre�a         VARCHAR2 (30) ,
    Poblacio       VARCHAR2 (30) ,
    Data_Neixement DATE ,
    Any_Arribada   DATE ,
    Titol          VARCHAR2 (50) ,
    Tit_Esportiu   VARCHAR2 (30) ,
    Mobil          VARCHAR2 (9) ,
    E_Mail         VARCHAR2 (50) ,
    Titulacio_Codi INTEGER NOT NULL
  ) ;
ALTER TABLE Staff ADD CONSTRAINT Staff_PK PRIMARY KEY ( Llicencia ) ;


CREATE TABLE Titulacio
  (
    Academica VARCHAR2 (50) ,
    Esportiva VARCHAR2 (50) ,
    Codi      INTEGER NOT NULL
  ) ;
ALTER TABLE Titulacio ADD CONSTRAINT Titulacio_PK PRIMARY KEY ( Codi ) ;


CREATE TABLE Treballan
  (
    Equips_Nom       INTEGER NOT NULL ,
    Equips_Sexe      CHAR (1 CHAR) NOT NULL ,
    Equips_Temporada VARCHAR2 (7) NOT NULL ,
    Persona_Licencia VARCHAR2 (50) NOT NULL ,
    Staff_Llicencia  VARCHAR2 (7) NOT NULL ,
    Jugadors_Dorsal  INTEGER NOT NULL
  ) ;


CREATE TABLE alternativa_pluja
  (
    Camps_Nom  VARCHAR2 (50) NOT NULL ,
    Camps_Nom1 VARCHAR2 (50) NOT NULL
  ) ;
ALTER TABLE alternativa_pluja ADD CONSTRAINT alternativa_pluja_PK PRIMARY KEY ( Camps_Nom, Camps_Nom1 ) ;


CREATE TABLE creacio_acta
  (
    Acta_Dorsal  VARCHAR2 (3) NOT NULL ,
    Acta_Quart   INTEGER NOT NULL ,
    Acta_Dorsal1 VARCHAR2 (3) NOT NULL ,
    Acta_Quart1  INTEGER NOT NULL
  ) ;
ALTER TABLE creacio_acta ADD CONSTRAINT creacio_acta_PK PRIMARY KEY ( Acta_Dorsal, Acta_Quart, Acta_Dorsal1, Acta_Quart1 ) ;


CREATE TABLE equip_entrena_camp
  (
    Equip_Nom  VARCHAR2 (50) NOT NULL ,
    Equip_Sexe VARCHAR2 (50) NOT NULL ,
    Camps_Nom  VARCHAR2 (50) NOT NULL
  ) ;
ALTER TABLE equip_entrena_camp ADD CONSTRAINT equip_entrena_camp_PK PRIMARY KEY ( Equip_Nom, Equip_Sexe, Camps_Nom ) ;


ALTER TABLE Arbitres ADD CONSTRAINT Arbitres_Partit_FK FOREIGN KEY ( Partit_Camp, Partit_Data ) REFERENCES Partit ( Camp, Data ) ;

ALTER TABLE Arbitres ADD CONSTRAINT Arbitres_Partit_FKv1 FOREIGN KEY ( Partit_Camp1, Partit_Data1 ) REFERENCES Partit ( Camp, Data ) ;

ALTER TABLE Categories ADD CONSTRAINT Categories_Equip_FK FOREIGN KEY ( Equip_Nom, Equip_Sexe ) REFERENCES Equip ( Nom, Sexe ) ;

ALTER TABLE Equip ADD CONSTRAINT Equip_Jugadors_FK FOREIGN KEY ( Jugadors_Dorsal ) REFERENCES Jugadors ( Dorsal ) ;

ALTER TABLE Equip ADD CONSTRAINT Equip_Partit_FK FOREIGN KEY ( Partit_Camp, Partit_Data ) REFERENCES Partit ( Camp, Data ) ;

ALTER TABLE Equip ADD CONSTRAINT Equip_Partit_FKv1 FOREIGN KEY ( Partit_Camp1, Partit_Data1 ) REFERENCES Partit ( Camp, Data ) ;

ALTER TABLE Equip ADD CONSTRAINT Equip_Staff_FK FOREIGN KEY ( Staff_Llicencia ) REFERENCES Staff ( Llicencia ) ;

ALTER TABLE Equips_Rivals ADD CONSTRAINT Equips_Rivals_Partit_FK FOREIGN KEY ( Partit_Camp, Partit_Data ) REFERENCES Partit ( Camp, Data ) ;

ALTER TABLE alternativa_pluja ADD CONSTRAINT FK_ASS_2 FOREIGN KEY ( Camps_Nom ) REFERENCES Camps ( Nom ) ;

ALTER TABLE alternativa_pluja ADD CONSTRAINT FK_ASS_3 FOREIGN KEY ( Camps_Nom1 ) REFERENCES Camps ( Nom ) ;

ALTER TABLE creacio_acta ADD CONSTRAINT FK_ASS_6 FOREIGN KEY ( Acta_Dorsal, Acta_Quart ) REFERENCES Acta ( Dorsal, Quart ) ;

ALTER TABLE creacio_acta ADD CONSTRAINT FK_ASS_7 FOREIGN KEY ( Acta_Dorsal1, Acta_Quart1 ) REFERENCES Acta ( Dorsal, Quart ) ;

ALTER TABLE equip_entrena_camp ADD CONSTRAINT FK_ASS_8 FOREIGN KEY ( Equip_Nom, Equip_Sexe ) REFERENCES Equip ( Nom, Sexe ) ;

ALTER TABLE equip_entrena_camp ADD CONSTRAINT FK_ASS_9 FOREIGN KEY ( Camps_Nom ) REFERENCES Camps ( Nom ) ;

ALTER TABLE Partit ADD CONSTRAINT Partit_Acta_FK FOREIGN KEY ( Acta_Dorsal, Acta_Quart ) REFERENCES Acta ( Dorsal, Quart ) ;

ALTER TABLE Staff ADD CONSTRAINT Staff_Titulacio_FK FOREIGN KEY ( Titulacio_Codi ) REFERENCES Titulacio ( Codi ) ;

ALTER TABLE Treballan ADD CONSTRAINT Treballan_Jugadors_FK FOREIGN KEY ( Jugadors_Dorsal ) REFERENCES Jugadors ( Dorsal ) ;

ALTER TABLE Treballan ADD CONSTRAINT Treballan_Persona_FK FOREIGN KEY ( Persona_Licencia ) REFERENCES Persona ( Licencia ) ;

ALTER TABLE Treballan ADD CONSTRAINT Treballan_Staff_FK FOREIGN KEY ( Staff_Llicencia ) REFERENCES Staff ( Llicencia ) ;


-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            15
-- CREATE INDEX                             0
-- ALTER TABLE                             32
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
