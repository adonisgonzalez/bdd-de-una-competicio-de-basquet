PROBLEMES SQL SOBRE LA BASE DE  DADES RESERVA DE VOLS

--1.  Totes les dades de tots els passatgers existents a la base de dades. 

SELECT * FROM PERSONA;

--2.  Nom dels passatgers que han volat almenys 5 vegades, ordenats pel nombre de vegades que han volat. 
--    Mostrar també el nombre de vegades que han volat com a n vegades.  

SELECT P.NOM, COUNT(B.CODI_VOL) AS N_VEGADES
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
GROUP BY P.NOM
HAVING COUNT(B.CODI_VOL) > 5
ORDER BY 1, 2

--3.  Nom dels passatgers vegetarians. 

SELECT P.NOM
FROM PERSONA P
WHERE P.OBSERVACIONS='Vegetaria/na'
ORDER BY 1;


--4.  Totes les dades de totes les maletes. 


--5.  Nom dels passatgers nascuts entre l'any 1960 i el 1990. 

-- 6.  Totes les dades de totes les reserves. 


--7.  Nom de totes les persones que són empresa. (Observació: el NIF d'una empresa conte 10 caràcters). 

-- 8.  Nom i telèfon de totes les persones portugueses. 


-- 9.  El codi i la data de tots els vols. 

-- 10. El nom i NIF de tots els passatgers. 

--11. El codi postal de les persones amb nacionalitat espanyola. 


-- 12. Localitzador(s) dels passatgers sense seient pel vol ANS5391. 

-- 13. Les diferents nacionalitats a les quals pertanyen els passatgers. 

--14. Nom  dels  passatgers  que  tenen  alguna  maleta  que  pesa  més  de 10  kilos. Especifica ara en quin vol (codi i data). 

SELECT P.NOM, M.CODI_VOL, M.DATA
FROM PERSONA P, MALETA M, BITLLET B
WHERE M.PES > 10
AND P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
ORDER BY 1

-- 15. Tots els models diferents davió que existeixen. 

--16. Tipus diferents davió (marca i model) que utilitza la companyia IBERIA en els seus vols. 
-- 17. Els codis de les maletes, ordenades per pes. 

--18. El nom i la nacionalitat de tots els passatgers, ordenats per nacionalitats (en ordre alfabètic decreixent). 

SELECT P.NOM, P.PAIS
FROM PERSONA P
ORDER BY 2, 1;

19. La fila i la lletra de totes les butaques assignades, ordenades per NIF. 
20. El codi de la maleta i el localitzador de la reserva de totes les maletes, ordenades per pes. 

21. Nom i fila dels passatgers asseguts a les files 2 i 3 dels 
vols amb codi VLG2111. 
22. Nom i edat de tots els passatgers. 
23. Nom dels passatgers que han nascut abans de lany 1975. 

--24. Edat dels passatgers del vol AEA2159. 

SELECT P.NOM, trunc((trunc(sysdate) - trunc(p.data_naixement))/365) as edad
FROM PERSONA P, BITLLET B
WHERE B.CODI_VOL='AEA2159'
AND P.NIF=B.NIF_PASSATGER
ORDER BY 1

25. Tots els vols amb data dabans de l1 de Juny de 2013. 
26. Codi i data dels vols BCN-LTN entre Març i Juny de 2013. 
--27. Per al vol amb data 02/06/14 i codi RAM964 digueu els NIF/CIF dels passatgers. 

SELECT P.NIF
FROM PERSONA P, BITLLET B
WHERE B.DATA = TO_DATE('02/06/2014','DD/MM/YYYY')
AND B.CODI_VOL= 'RAM964'
AND P.NIF=B.NIF_PASSATGER
ORDER BY 1

28. Les companyies amb vols durant el mes dagost de 2013. 
29. Les diferents dates en les quals hi ha vols de la companyiaIBERIA. 
30. Les diferents companyies que tenen vol el dia 20 dAgost de 2013. 
31. Nom dels passatgers que són menors dedat a dia davui. 
32. Localitzador de les reserves dels vols amb data entre Març i Setembre de 2013. 
33. Nom dels passatgers amb butaques a les tres primeres files.
--34. Nom de les companyies en les que, en algun dels seus vols, hi viatgi un passatger que menja Kosher. 

SELECT DISTINCT V.COMPANYIA
FROM PERSONA  P, BITLLET B, VOL V
WHERE P.OBSERVACIONS='Kosher'
AND P.NIF=B.NIF_PASSATGER
AND B.CODI_VOL=V.CODI_VOL
AND B.DATA=V.DATA
ORDER BY 1

35. Localitzador(s)  dels  passatgers  embarcats  al  vol  dAir  Euro
pa AEA2195  del  dia 
29/08/13. 
36. Nom  dels  passatgers  amb  reserves  fetes  al  seient  amb  lletra
 A o B 
(independentment de la fila). 
37. País dels passatgers que viatgen a primera classe. 
38. Codi de vol i data dels viatges del passatger Alan Brito. 
--39. El codi de la maleta i el pes de les maletes que ha facturat el passatger Jose Luis Lamata Feliz en els seus vols. 

SELECT M.CODI_MALETA, M.PES
FROM MALETA M, PERSONA P, BITLLET B
WHERE P.NOM='Jose Luis Lamata Feliz'
AND P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
ORDER BY 1

40. El codi de la maleta i el pes de les maletes que ha facturat la passatgera Mireia Baila Sola en el vol AF2352 del dia 31/10/2013. 
41. Companyies dels vols reservats pel passatger Alberto Mate, sense repeticions. 
42. Butaques (fila i lletra) que ha reservat la passatgera Encarna Vales en els seus vols. 
43. NIF i nom dels passatgers que tenen una reserva al vol RYR6341 del 30/10/2013. 
44. Butaques (fila i lletra) reservats en el vol dIberia IBE2115 del 7 de Novembre de 2013. 
45. Tipus davions en que ha viatjat la passatgera Rosa Flores Rojas. 
46. La fila i la lletra de les butaques reservades per passatgers espanyols. Cal mostrar també el nom dels passatgers i el codi i la data dels respectius vols. 
47. Nom dels passatgers que tenen alguna maleta que pesa més de 10 kilos. 
48. Els codis de vol i les dates de les reserves dels passatgers majors de 50 anys a dia davui. Mostrar també els noms respectius. 
49. Nombre màxim de bitllets (com a max_bitllets) que ha comprat en una sola reserva lempresa Consultoria Marchese. 
50. Les  companyies  dels  avions  dels  passatgers  espanyols  nascuts  abans  de  lany 1960 (*). 
51. Codis dels vols en els quals hi ha algun passatger menor dedat. 
52. Companyies que tenen algun vol sense cap passatger. 
53. Nombre de passatgers (com a n_passatgers) amb cognom Blancoque té el vol amb codi IBE2119 i data 30/10/13. 
54. Tipus davió amb més files. 
55. Nom dels passatgers que tenen alguna reserva feta (fer-la amb exists). 
56. Nom de les persones que han nascut entre el 05/06/1964 i el 03/09/1985. 
--57. Nombre  de  butaques  reservades  (com  a  n_butaques_reservades)al  vol  de LUFTHANSA DLH1807 del 4 dOctubre de 2013. 

SELECT COUNT(B.LLETRA) AS N_BUTAQUES
FROM BITLLET B, PERSONA P
WHERE B.CODI_VOL= 'DLH1807'
AND B.DATA = TO_DATE('04/10/2013','DD/MM/YYYY')
AND P.NIF=B.NIF_PASSATGER
GROUP BY P.NIF
ORDER BY 1

58. Pes total facturat (com a pes_total) pel vol de KLM KLM2699de l1 dOctubre de 2013. 
59. Aforament (nombre de butaques total com a n_butaques) del vol de LUFTHANSA DLH1807 del 4 dOctubre de 2013. 
60. Codi  i  data  dels  vols  que  tenen  més  de  3  reserves,  ordenatspel  nombre  de reserves. 
61. Percentatge  docupació  (com  a  p_ocupacio)  del  vol  dIberia IBE2119  del  30 dOctubre de 2013. 
62. El pes de les maletes (com a pes_total) dels passatgers italians. 
63. El nombre de persones mexicanes com a n_mexicans. 
--64. Pes total de les maletes dels passatgers jubilats (majors de 65 anys) a dia davui.

SELECT SUM(M.PES) AS PES_TOTAL
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND trunc((trunc(sysdate) - trunc(p.data_naixement))/365) > 65
ORDER BY 1;

65. Nombre total de bitllets (com a n_bitllets) que ha reservatlempresa Consultoria Marchese. 
66. Nombre  de  companyies  (com  a  n_companyies)  que  tenen  vols  amb  destinació Paris Orly Airport. 

--67. Nombre  de  maletes  que  ha  facturat  Chema  Pamundi  per  cada  vol  que  ha  fet. Atributs: codi del vol, data i nombre de maletes. 

SELECT b.codi_vol, B.DATA, COUNT(M.CODI_MALETA) AS N_MALETES
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NOM = 'Chema Pamundi'
AND P.NIF=B.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
AND B.NIF_PASSATGER=M.NIF_PASSATGER
GROUP BY B.CODI_VOL, B.DATA
ORDER BY 1, 2, 3;


--68. Pes total de les maletes que ha facturat Chema Pamundi per cada vol que ha fet. Atributs: codi del vol, data i pes total. 
-- 69. Pes total facturat per a cada vol dIBERIA. Atributs: codi del vol, data i pes total. 
--70. Nombre de passatgers dels vols dIBERIA per països. Atributs: país i nombre de passatgers com a n_passatgers. 
--71. Nombre de vegades que apareix la nacionalitat més freqüent entre les persones de la BD. Atributs: nacionalitat i nombre de vegades com a n_vegades. 
--72. El  total  de  pes  que  aporta  cada  país  al  total  de  vols,  ordenant  els  països  per aquesta quantitat de pes. Atributs: país, pes_total. 
--73. Nom dels passatgers que també han fet alguna reserva. 

SELECT DISTINCT P.NOM
FROM PERSONA P, BITLLET B, RESERVA R
WHERE P.NIF=B.NIF_PASSATGER
AND P.NIF=R.NIF_CLIENT
AND B.LOCALITZADOR=R.LOCALITZADOR
ORDER BY 1;


--74. Nombre de butaques ofertades per cada vol i dia i hores de sortida i arribada. 
--75. El nombre de vols per cada companyia. Atributs de sortida: companyia, n_vols. 
--76. Nombre de butaques per fila que ha reservat el passatger Aitor Tilla en els seus vols. 
--77. NIF i nom dels passatgers que han facturat més de 15 kgs. de maletes en algun dels seus vols. Atributs: NIF, nom, codi_vol, data i pes_total.
--78. Nacionalitats representades per menys de quatre passatgers.Atributs: nacionalitat, n_vegades. 

SELECT P.PAIS, COUNT(P.NOM) AS N_PASSATGERS
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
GROUP BY P.PAIS
HAVING COUNT(P.PAIS) < 4
ORDER BY 1

--79. Nacionalitats representades per més de dos passatgers amb bitllets. 
--80. NIF i pes total (com a pes_total) de les maletes dels passatgers que han facturat més de 12 kgs. en el total de les seves reserves. 
--81. NIF de les persones que han reservat més de dos bitllets. 
--82. Codi i data dels vols que han facturat, en total, un pes igual o superior a 260 kgs. Especifiqueu també el pes_total.

SELECT B.CODI_VOL, B.DATA, SUM(M.PES)
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY B.CODI_VOL, B.DATA
HAVING SUM(M.PES) >= 260
ORDER BY 1;


--83. Les nacionalitats que, en el conjunt de tots els vols, facturin un pes igual o major a 50 kgs. Atributs: país i pes_total. 

-- 84. NIF i nom dels passatgers que han facturat una sola maleta en algun dels seus vols. 

SELECT DISTINCT P.NIF, P.NOM
FROM PERSONA P, MALETA M, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY  B.CODI_VOL, P.NIF, P.NOM
HAVING COUNT(*) =1
ORDER BY 1

85. Codis  dels  vols  (independentment  de  la  data)  que  tenen  alme nys  un  passatger jubilat (edat >= 65). 
86. Data i codi de vol dels vols amb més de 3 passatgers. 

--87. NIF i nom del(s) passatger(s) que ha(n) facturat més maletes en els seus vols. 

SELECT P.NIF, P.NOM
FROM PERSONA P, MALETA M, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY P.NIF, P.NOM
HAVING COUNT(M.CODI_MALETA) >= ALL ( 
                                    SELECT COUNT(M.CODI_MALETA)
                                    FROM PERSONA P, MALETA M, BITLLET B
                                    WHERE P.NIF=B.NIF_PASSATGER
                                    AND B.NIF_PASSATGER=M.NIF_PASSATGER
                                    AND B.CODI_VOL=M.CODI_VOL
                                    AND B.DATA=M.DATA
                                    GROUP BY P.NIF, P.NOM)
                                    
ORDER BY 1, 2;



88. Número  de  porta  amb  més  d'un  vol  assignat.  Mostrar  codi  de l'aeroport  al  que pertany, terminal, àrea i porta. 
89. Nacionalitat(s) que apareix(en) més vegades a la BD. Mostrar el país i el nombre de vegades que apareix. 
90. NIF i nom del(/s) comprador(/s) que ha(n) reservat més bitllets. 
91. NIF i nom del(s) passatger(s) que ha(n) facturat menys pes en els seus vols. 
92. Butaca(ques) preferida(/es) (més seleccionada) pels passatgers. 
93. Nom del(s) aeroport(s) amb més terminals. 
94. Nom  del(s)  aeroport(s)  amb  més  portes  dembarcament  per  unaterminal  i  àrea concretes. 

--95. Nom  de  la(/es)  companyia(/es)  que  té(nen)  més  butaques  rese rvades  en  algun dels seus vols. 
--96. Models d'avió(ns) que té(nen) més passatgers no espanyols. 
    OK
    SELECT V.TIPUS_AVIO, P.PAIS, COUNT(P.PAIS)
    FROM VOL V, PERSONA P, BITLLET B
    WHERE P.NIF=B.NIF_PASSATGER
    AND B.CODI_VOL=V.CODI_VOL
    AND B.DATA=V.DATA
    GROUP BY V.TIPUS_AVIO, P.PAIS
    ORDER BY 1

97. Nom de la(/es) companyia(/es) que ha(n) estat reservada(/es
) per més clients de 
Cadis. 
98. Nom dels passatgers que han facturat alguna maleta. 
99. Companyia(/es) davions més sol·licitada(/es) (més reservad
a(/es)). 
100.   Model davió(ns) menys reservat(s) i nombre de vegades que
 sha reservat. 
101.   Comprador (Nom) que ha reservat més butaques en una sola c
ompra. 
102.   Nom del(s) aeroport(s) amb el mínim de terminals possible.
103.   Nom dels passatgers que tenen bitllet, però no han fet mai
 cap reserva. 
104.   Per al vol AEA2195 amb data 31/07/13, quin és el percentatge docupació? 
105.   Localitzador de la reserva més cara. 
106.   Quin és el nombre màxim de passatgers inclosos en una sola reserva feta pel client Bar 20 pa K? 
107.   Butaques disponibles (fila i lletra) pel vol AEA2195 amb data 31/07/13. 
108.   Número de places lliures pel vol AEA2195 amb data 31/07/13.
109.   Terminals  lliures  del  Prat  els  dilluns  a  les  10:00  (s’entenen  per  ocupades aquelles que tenen vols assignats pel dia Dilluns i hora 10:00). 
110.   Portes lliures de la terminal 1 del Prat pels Dimarts a les 16:00. 
111.   Dades  de contacte  (nom,  email,  telf)  dels  passatgers  que  no  volen  mai  a  la Xina. 
112.   Passatger que ha facturat a tots el seus vols.  
113.   Companyia que vola a tots els aeroports espanyols. 
114.   Destí dels vols que estan plens. 
115.   Passatger que ha volat amb tots els vols que van a l’aeroport del Prat. 
116.   Passatger que sempre vola amb primera classe. 
117.   Percentatge de passatgers espanyols al vol IB2119 del 30/10/2013. 
--118.   Percentatge  d’ocupació  per  nacionalitats  al  vol  IB2119  del 30/10/2013  i AEA2195 del 31/07/2013. 
119.   Nom de les persones que sempre paguen amb VISA. 
120.   Promig d’ocupació dels vols d’IBERIA al 2013. 
121.   Passatger que no ha facturat cap maleta al Vol KLM2699 a data 01/10/2013. 
