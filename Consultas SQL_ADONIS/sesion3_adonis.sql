--1)Numero de passatgers (com a N_Passatgers) amb cognom “Blanco” que té el vol amb codi IBE2119 i data 30/10/13. 

SELECT COUNT(*) AS N_Passatgers
from PERSONA P, BITLLET B
WHERE P.NOM like '%Blanco'
AND B.CODI_VOL='IBE2119'
AND B.DATA=TO_DATE('30/10/2013','DD/MM/YYYY')
AND P.NIF=B.NIF_PASSATGER
ORDER BY 1;

--2)Numero de  butaques  reservades  (com  a  N_Butaques_Reservades)  al  vol  de  VUELING AIRLINES VLG2117 del 27 de Agosto de 2013
select COUNT(*) AS N_Butaques_Reservades
from seient s
WHERE S.DATA=TO_DATE('27/08/2013','DD/MM/YYYY')
AND S.NIF_PASSATGER IN (SELECT S.NIF_PASSATGER FROM SEIENT S WHERE S.CODI_VOL='VLG2117');


--3)Pes total facturat (com a Pes_Total) pel vol de KLM KLM4304 de l'9 d'Octubre de 2013 
SELECT SUM(M.PES) AS PES_TOTAL
FROM MALETA M, BITLLET B
WHERE B.CODI_VOL='KLM4304'
AND B.DATA =TO_DATE('09/10/2013','DD/MM/YYYY')
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
ORDER BY 1;

--4)El pes de les maletes (com a Pes_Total) dels passatgers italians. 
SELECT SUM(M.PES) AS PES_TOTAL
FROM MALETA M, BITLLET B, PERSONA P
WHERE P.PAIS='Italia'
AND P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
ORDER BY 1;

--5) Numero màxim  de  bitllets  (com  a  max  bitllets)  que  ha  comprat  en  una  sola  reserva en Narciso Blanco?
SELECT MAX(COUNT(B.CODI_VOL)) AS MAX_BITLLETS
FROM PERSONA P, BITLLET B
WHERE P.NOM='Narciso Blanco'
AND P.NIF=B.NIF_PASSATGER
GROUP BY B.LOCALITZADOR
ORDER BY 1;

--6)Nom de la companyia i nombre de vols que té (as N_VOLS).  
SELECT V.COMPANYIA, COUNT(V.CODI_VOL) AS N_VOLS
FROM VOL V
GROUP BY V.COMPANYIA
ORDER BY 1;

--7) Nom de les Companyies amb mès de 10 vols.
SELECT V.COMPANYIA
FROM VOL V
HAVING COUNT(V.CODI_VOL) > 10
GROUP BY V.COMPANYIA
ORDER BY 1;


--8)Nom  dels  passatgers  que  han  volat almenys  5  vegades,  ordenats  pel  nombre de vegades que han volat. Mostrar també el nombre de vegades que han volat com a N_VEGADES.

SELECT P.NOM, COUNT(B.CODI_VOL) AS N_VEGADES
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
HAVING COUNT(*) >= 5
GROUP BY P.NOM
ORDER BY 2, 1;


--9)  NIF  i  nom  dels  passatgers  que  han  facturat  més  de  15  kgs.  de  maletes  en  algun dels seus vols. Atributs: NIF, nom, codi_vol, data i pes_total
SELECT P.NIF, P.NOM, M.CODI_VOL, M.DATA, SUM(M.PES) AS PES_TOTAL
FROM MALETA M, PERSONA P, BITLLET B
WHERE M.PES > 15
AND P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY P.NIF, P.NOM, M.CODI_VOL, M.DATA
ORDER BY 1, 2, 3, 4, 5;

--10) Nacionalitats representades per més de dos passatgers amb bitllets.
SELECT P.PAIS
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
HAVING COUNT(B.CODI_VOL) > 2
GROUP BY P.PAIS
ORDER BY 1;
