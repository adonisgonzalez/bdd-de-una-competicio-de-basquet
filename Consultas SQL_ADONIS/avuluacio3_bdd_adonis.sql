--1	(Reserves Vol) Nom del passatger/s que ha/n facturat la maleta de més pes.

SELECT DISTINCT P.NOM
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.DATA=M.DATA
AND B.CODI_VOL=M.CODI_VOL
AND M.PES = (SELECT MAX(M.PES) FROM MALETA M)
ORDER BY 1;


--2	(Reserves Vol) Color(s) de la(/es) maleta(/es) facturada(/es) més pesada(/es).

SELECT DISTINCT M.COLOR
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER 
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
AND M.PES=(SELECT MAX(M.PES) FROM MALETA M)
ORDER BY 1;

--3	(Reserves Vol) NIF, nom del(s) passatger(s) i pes total que ha(n) facturat menys pes en el conjunt de tots els seus bitllets.

CERBERO NO LA ACEPTA COMO BUENA (AUNQUE CREO QUE YO ES LA CORRECTA)

SELECT P.NIF, P.NOM, SUM(M.PES) AS PES_TOTAL
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY P.NIF, P.NOM
HAVING SUM(M.PES) <= ALL(SELECT SUM(M.PES)
                        FROM PERSONA P, BITLLET B, MALETA M
                        WHERE P.NIF=B.NIF_PASSATGER
                        AND B.NIF_PASSATGER=M.NIF_PASSATGER
                        AND B.CODI_VOL=M.CODI_VOL
                        AND B.DATA=M.DATA
                        GROUP BY P.NIF, P.NOM)
ORDER BY 1;


CERBERO LA ACEPTA COMO VALIDA 

select
        p.nif, p.nom, sum(m.pes) as pes_total
from
        persona p, bitllet b, maleta m
where
        p.nif=b.nif_passatger
        and b.nif_passatger=m.nif_passatger and b.codi_vol=m.codi_vol and b.data=m.data
group by
        p.nif, p.nom
order by 1,2,3;


--4	(Reserves Vol) Nom de la(/es) companyia(/es) que ha(n) estat reservada(/es) per més clients (persones) d'Amsterdam.

SELECT V.COMPANYIA
FROM PERSONA P, BITLLET B, RESERVA R, VOL V
WHERE P.POBLACIO='Amsterdam'
AND P.NIF IN ( SELECT R.NIF_CLIENT FROM RESERVA R)
AND P.NIF=B.NIF_PASSATGER
AND P.NIF=R.NIF_CLIENT
AND B.CODI_VOL=V.CODI_VOL
AND B.DATA=V.DATA
AND B.LOCALITZADOR=R.LOCALITZADOR
GROUP BY V.COMPANYIA
HAVING COUNT(P.NIF) >= ALL ( SELECT COUNT(P.NIF)
                            FROM PERSONA P, BITLLET B, RESERVA R, VOL V
                            WHERE P.POBLACIO='Amsterdam'
                            AND P.NIF IN ( SELECT R.NIF_CLIENT FROM RESERVA R)
                            AND P.NIF=B.NIF_PASSATGER
                            AND P.NIF=R.NIF_CLIENT
                            AND B.CODI_VOL=V.CODI_VOL
                            AND B.DATA=V.DATA
                            AND B.LOCALITZADOR=R.LOCALITZADOR
                            GROUP BY V.COMPANYIA)
ORDER BY 1;

--5	(Reserves Vol) Nom del(s) aeroport(s) amb el mínim número de terminals.

(YO CREO QUE ES CORRECTO AUNQUE CERBERO NO LA DE POR VALIDA)

SELECT DISTINCT A.NOM, MAX(P.TERMINAL)
FROM AEROPORT A, PORTA_EMBARCAMENT P
WHERE A.CODI_AEROPORT=P.CODI_AEROPORT
GROUP BY A.NOM
HAVING MAX(P.TERMINAL) <= ALL (SELECT MAX(P.TERMINAL)
                              FROM PORTA_EMBARCAMENT P, AEROPORT A
                              WHERE A.CODI_AEROPORT=P.CODI_AEROPORT
                              GROUP BY A.NOM)
ORDER BY 1;



(CERBERO DA POR VALIDA)

select
        distinct a.nom
from
        aeroport a, porta_embarcament p
where
        a.codi_aeroport=p.codi_aeroport
group by
        a.nom, p.terminal
having
        count(*)<=ALL(select
                                    count(*)
                            from
                                    aeroport a, porta_embarcament p
                            where
                                    a.codi_aeroport=p.codi_aeroport
                            group by
                                    a.nom, p.terminal
                            )
order by 1;


--6	(Espectacles) Nom del recinte/s amb entrades a preu més alt. Ordenat per nom del recinte.

solución buena para mi (busca en todas las entradas que se ha vendido)

SELECT RE.NOM
FROM RECINTES RE, PREUS_ESPECTACLES PRE, ENTRADES EN, REPRESENTACIONS REP, ESPECTACLES E
WHERE RE.CODI=PRE.CODI_RECINTE
AND EN.CODI_ESPECTACLE=REP.CODI_ESPECTACLE
AND EN.DATA=REP.DATA
AND EN.HORA=REP.HORA
AND REP.CODI_ESPECTACLE=E.CODI
GROUP BY RE.NOM
HAVING MAX(PRE.PREU) >= ALL (SELECT MAX(PRE.PREU) 
                              FROM RECINTES RE, PREUS_ESPECTACLES PRE, ENTRADES EN, REPRESENTACIONS REP, ESPECTACLES E
                              WHERE RE.CODI=PRE.CODI_RECINTE
                              AND EN.CODI_ESPECTACLE=REP.CODI_ESPECTACLE
                              AND EN.DATA=REP.DATA
                              AND EN.HORA=REP.HORA
                              AND REP.CODI_ESPECTACLE=E.CODI
                              GROUP BY RE.NOM)
ORDER BY 1;


subida a CERBERO

select
        r.nom
from
        recintes r, preus_espectacles p
where
        r.codi=p.codi_recinte
group by
        r.nom
having
        max(p.preu)>=ALL(select
                                  max(p.preu)
                          from
                                  recintes r, preus_espectacles p
                          where
                                  r.codi=p.codi_recinte
                          group by
                                  r.nom
                          )
order by 1

--7	(Espectacles) Zona/es amb la menor capactitat. Mostra el nom del recinte i la zona.

SELECT DISTINCT RE.NOM, ZO.ZONA
FROM RECINTES RE, ZONES_RECINTE ZO
WHERE RE.CODI=ZO.CODI_RECINTE
GROUP BY ZO.ZONA, RE.NOM
HAVING MIN(ZO.CAPACITAT) <= ALL ( SELECT MIN(ZO.CAPACITAT)
                                  FROM RECINTES RE, ZONES_RECINTE ZO
                                  WHERE RE.CODI=ZO.CODI_RECINTE
                                  GROUP BY ZO.ZONA, RE.NOM)
ORDER BY 1, 2;

--8	(Espectacles) Nom de l'espectacle amb més espectadors

SELECT E.NOM
FROM ESPECTACLES E, REPRESENTACIONS REP, ENTRADES EN
WHERE EN.CODI_ESPECTACLE=REP.CODI_ESPECTACLE
AND EN.DATA=REP.DATA
AND EN.HORA=REP.HORA
AND REP.CODI_ESPECTACLE=E.CODI
GROUP BY E.NOM
HAVING COUNT(EN.DNI_CLIENT) >= ALL ( SELECT COUNT(EN.DNI_CLIENT)
                              FROM ESPECTACLES E, REPRESENTACIONS REP, ENTRADES EN
                              WHERE EN.CODI_ESPECTACLE=REP.CODI_ESPECTACLE
                              AND EN.DATA=REP.DATA
                              AND EN.HORA=REP.HORA
                              AND REP.CODI_ESPECTACLE=E.CODI
                              GROUP BY E.NOM)
ORDER BY 1;


--9	(Espectacles) Zona del "Liceu" més ocupada el 2012.

SELECT EN.ZONA
FROM ENTRADES EN, RECINTES RE, BUTAQUES BU, ZONES_RECINTE ZO
WHERE RE.NOM='Liceu'
AND EN.DATA >= TO_DATE('01/01/2012','DD/MM/YYYY')
AND EN.DATA < TO_DATE('01/01/2013','DD/MM/YYYY')
AND EN.CODI_RECINTE=BU.CODI_RECINTE
AND EN.ZONA=BU.ZONA
AND EN.FILA=BU.FILA
AND EN.NUMERO=BU.FILA
AND BU.CODI_RECINTE=ZO.CODI_RECINTE
AND BU.ZONA=ZO.ZONA
AND ZO.CODI_RECINTE=RE.CODI
GROUP BY EN.ZONA
HAVING COUNT(EN.DNI_CLIENT) >= ALL (SELECT COUNT(EN.DNI_CLIENT)
                                    FROM ENTRADES EN, RECINTES RE, BUTAQUES BU, ZONES_RECINTE ZO
                                    WHERE RE.NOM='Liceu'
                                    AND EN.DATA >= TO_DATE('01/01/2012','DD/MM/YYYY')
                                    AND EN.DATA < TO_DATE('01/01/2013','DD/MM/YYYY')
                                    AND EN.CODI_RECINTE=BU.CODI_RECINTE
                                    AND EN.ZONA=BU.ZONA
                                    AND EN.FILA=BU.FILA
                                    AND EN.NUMERO=BU.FILA
                                    AND BU.CODI_RECINTE=ZO.CODI_RECINTE
                                    AND BU.ZONA=ZO.ZONA
                                    AND ZO.CODI_RECINTE=RE.CODI
                                    GROUP BY EN.ZONA)
ORDER BY 1;


--10	(Espectacles) Espectador (DNI,Nom,Cognoms) que més entrades ha comprat per l'espectacle de 'El Màgic d'Oz'.

SELECT ESP.DNI, ESP.NOM, ESP.COGNOMS
FROM ESPECTADORS ESP, ENTRADES EN, REPRESENTACIONS RE, ESPECTACLES E
WHERE E.NOM LIKE 'El M‡gic%'
AND ESP.DNI=EN.DNI_CLIENT
AND EN.CODI_ESPECTACLE=RE.CODI_ESPECTACLE
AND EN.DATA=RE.DATA
AND EN.HORA=RE.HORA
AND RE.CODI_ESPECTACLE=E.CODI
GROUP BY ESP.DNI, ESP.NOM, ESP.COGNOMS
HAVING COUNT(E.CODI) >= ALL(SELECT COUNT(E.CODI)
                            FROM ESPECTADORS ESP, ENTRADES EN, REPRESENTACIONS RE, ESPECTACLES E
                            WHERE E.NOM LIKE 'El M‡gic%'
                            AND ESP.DNI=EN.DNI_CLIENT
                            AND EN.CODI_ESPECTACLE=RE.CODI_ESPECTACLE
                            AND EN.DATA=RE.DATA
                            AND EN.HORA=RE.HORA
                            AND RE.CODI_ESPECTACLE=E.CODI
                            GROUP BY ESP.DNI, ESP.NOM, ESP.COGNOMS)
ORDER BY 1, 2, 3;