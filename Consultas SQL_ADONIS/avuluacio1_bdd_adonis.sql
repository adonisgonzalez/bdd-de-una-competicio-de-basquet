--1   (Reserves vols) NIF, nom, població i país de totes les persones existents a la base de dades.

SELECT P.NIF, P.NOM, P.POBLACIO, P.PAIS
FROM PERSONA P
ORDER BY 1;

--2	(Reserves Vol) El codi de la maleta i el localitzador de la reserva de totes les maletes.

SELECT M.CODI_MALETA, B.LOCALITZADOR
FROM BITLLET B, MALETA M
WHERE B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
ORDER BY 1, 2;


--3	(Reserves Vol) Nom dels passatgers que han nascut abans de l any 1975

SELECT P.NOM
FROM PERSONA P
WHERE P.DATA_NAIXEMENT <= TO_DATE('01/01/1975','DD/MM/YYYY')
ORDER BY 1;

--4	(Reserves Vols) Codi de tots els vols amb bitllets amb data d'abans de l'1 de Desembre de 2013.

SELECT DISTINCT B.CODI_VOL
FROM BITLLET B
WHERE B.DATA < TO_DATE('01/12/2013','DD/MM/YYYY')
ORDER BY 1;


--5	(Reserves Vols) Localitzador(s) i nom de les persones pel vol KLM4303.

SELECT B.LOCALITZADOR, P.NOM
FROM BITLLET B, PERSONA P
WHERE B.CODI_VOL='KLM4303'
AND P.NIF=B.NIF_PASSATGER
ORDER BY 1, 2;


--6	(Espectacles) Per cadascuna de les zones del teatre on es representa La extraña pareja, nom de la zona, capacitat i preu de les entrades.

SELECT Z.ZONA, Z.CAPACITAT, P.PREU
FROM ZONES_RECINTE Z, PREUS_ESPECTACLES P, ESPECTACLES E
WHERE E.NOM='La extraÒa pareja'
AND Z.CODI_RECINTE=P.CODI_RECINTE
AND Z.ZONA=P.ZONA
AND P.CODI_ESPECTACLE=E.CODI
ORDER BY 1, 2, 3;

--7	(Espectacles) Nom, cognoms i adreça dels espectadors que han adquirit alguna entrada per la representació del dia 20 de novembre del 2011 
a les 21h de West Side Story.

SELECT DISTINCT E.NOM, E.COGNOMS, E.ADREÁA
FROM ESPECTADORS E, ENTRADES EN, ESPECTACLES ES, REPRESENTACIONS R
WHERE EN.DATA=TO_DATE('20/11/2011','DD/MM/YYYY')
AND TO_CHAR(R.HORA,'HH24:MI:SS')='21:00:00'
AND ES.NOM='West Side Story'
AND E.DNI=EN.DNI_CLIENT
AND EN.CODI_ESPECTACLE=R.CODI_ESPECTACLE
AND EN.DATA=R.DATA
AND EN.HORA=R.HORA
AND R.CODI_ESPECTACLE=ES.CODI
ORDER BY 1, 2, 3;


--8	(Espectacles) Entrades (nom de l. espectacle, data (amb format: DD/MM/YYYY), zona, fila i número) adquirides per clients de 
Barcelona per les representacions del mes de febrer del 2012 en el teatre Romea.

SELECT E.NOM, TO_CHAR(EN.DATA,'DD/MM/YYYY')AS DATA, EN.ZONA, EN.FILA, EN.NUMERO
FROM ESPECTACLES E, REPRESENTACIONS R, ENTRADES EN, ESPECTADORS ES, RECINTES RE
WHERE ES.CIUTAT='Barcelona'
AND EN.DATA >= TO_DATE('01/02/2012','DD/MM/YYYY')
AND EN.DATA < TO_DATE('01/03/2012','DD/MM/YYYY')
AND RE.NOM='Romea'
AND ES.DNI=EN.DNI_CLIENT
AND EN.CODI_ESPECTACLE=R.CODI_ESPECTACLE
AND EN.DATA=R.DATA
AND EN.HORA=R.HORA
AND R.CODI_ESPECTACLE=E.CODI
AND E.CODI_RECINTE=RE.CODI
ORDER BY 1, 2, 3, 4, 5;


--9	(Espectacles) Butaques (zona, fila i número) del recinte on es representa Hamlet.

SELECT DISTINCT BU.ZONA, BU.FILA, BU.NUMERO
FROM ESPECTACLES ES, RECINTES RE, ZONES_RECINTE ZO, BUTAQUES BU
WHERE ES.NOM='Hamlet'
AND ES.CODI_RECINTE=RE.CODI
AND RE.CODI=ZO.CODI_RECINTE
AND ZO.CODI_RECINTE=BU.CODI_RECINTE
AND ZO.ZONA=BU.ZONA
ORDER BY 1, 2, 3;


--10	(Espectacles) Nom i cognoms dels espectadors que han assistit a l'espectacle "Entre Tres".


(PREGUNTAR SI ES VALIDA)

SELECT DISTINCT ES.NOM, ES.COGNOMS
FROM ESPECTADORS ES, ESPECTACLES ESP, ENTRADES EN, REPRESENTACIONS R
WHERE ES.DNI IN (SELECT EN.DNI_CLIENT 
                 FROM ENTRADES EN, ESPECTADORS ES, ESPECTACLES ESP, REPRESENTACIONS R
                 WHERE  ESP.NOM='Entre Tres'
                 AND ES.DNI=EN.DNI_CLIENT
                 AND EN.CODI_ESPECTACLE=R.CODI_ESPECTACLE
                 AND EN.DATA=R.DATA
                 AND EN.HORA=R.HORA
                 AND R.CODI_ESPECTACLE=ESP.CODI)
ORDER BY 1, 2;