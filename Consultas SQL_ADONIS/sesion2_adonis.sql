--1.El nom i NIF de tots els passatgers.
select p.nom, p.nif
from persona p
order by 1,2;

--2) Nom dels passatgers vegetarians
select p.nom
from persona p
where p.observacions='Vegetaria/na'
order by 1;

--3) Codi de vol i data dels bitllets del passatger Alan Brito
select b.codi_vol, b.data
from bitllet b, persona p
where p.NOM='Alan Brito'
and p.nif=b.NIF_PASSATGER;

--4) Nom  dels  passatgers  que  tenen  alguna  maleta  quepesa  més  de  10  kilos. Especifica també en quin vol
select distinct p.nom, m.codi_vol
from persona p, maleta m, bitllet b
where m.pes > 10
and p.nif=b.nif_passatger
and b.nif_passatger=m.nif_passatger
and b.data=m.data
and b.codi_vol=m.codi_vol
order by 1, 2;

--5) El codi de la maleta, el pes i les mides de les maletes que ha facturat el passatger Jose Luis Lamata Feliz en els seus vols.
select m.codi_maleta, m.pes, m.mides
from maleta m, persona p, bitllet b
where p.nom='Jose Luis Lamata Feliz'
and p.nif=b.nif_passatger
and b.nif_passatger=m.nif_passatger
and b.data=m.data
and b.codi_vol=m.codi_vol;

--6)Nom de les passatgers que han nascut entre el 05/06/1964 i el 03/09/1985
select p.nom, p.data_naixement 
from persona p
where p.data_naixement > to_date('05/06/1964','DD/MM/YYYY')
and p.data_naixement < to_date('03/09/1985','DD/MM/YYYY');


--7) Per al vol amb data 02/06/14 i codi RAM964 digueu el NIF dels passatgers que volen i el dels que han comprat el bitllet.
select b.nif_passatger, r.nif_client
from bitllet b, reserva r
where b.data=to_date('02/06/2014','DD/MM/YYYY')
and b.codi_vol='RAM964'
and b.localitzador=r.localitzador
order by 1, 2;

-- 8. Nom i edat dels passatgers del vol AEA2159 del 25/07/2013.
select p.nom, trunc((trunc(sysdate) - trunc(p.data_naixement))/365) as edad
from persona p, bitllet b
where b.codi_vol='AEA2159'
and b.data=to_date('25/07/2013','DD/MM/YYYY')
and p.nif=b.nif_passatger
order by 1, 2;

--9)Tots els models diferents d'avió que existeixen
select distinct v.tipus_avio
from vol v
order by 1;


--10) Les companyies amb vols al mes d'Agost de 2013.
select distinct v.companyia
from vol v
where v.data >= to_Date('01/08/2013','dd/mm/yyyy')
and v.data <= to_Date('31/08/2013','dd/mm/yyyy')
order by 1;s