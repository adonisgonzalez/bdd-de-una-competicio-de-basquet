--1	CORRECTE(Aerolinies) Pes total de les maletes facturades per passatgers espanyols.

SELECT SUM(M.PES) AS pes_total
FROM MALETA M, PERSONA P, BITLLET B
WHERE P.PAIS='Espanya'
AND P.NIF=B.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
AND B.NIF_PASSATGER=M.NIF_PASSATGER
ORDER BY 1;

-- 2	CORRECTE(Aerolinies) Codi, data i pes total dels vols que han facturat, en total, un pes igual o superior a 26 kgs.

SELECT B.CODI_VOL, TO_CHAR(B.DATA,'DD/MM/YYYY'), SUM(M.PES) 
FROM MALETA M, BITLLET B
WHERE B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
AND B.NIF_PASSATGER=M.NIF_PASSATGER
GROUP BY B.CODI_VOL, B.DATA
HAVING SUM(M.PES)>=26
ORDER BY 1, 2, 3

--3	CORRECTE(Aerolinies) Nombre de passatgers dels vols d'IBERIA per països. Es demana país i nombre de passatgers.

SELECT DISTINCT P.PAIS, COUNT(P.NOM) AS nombre_passatgers
FROM PERSONA P, BITLLET B, VOL V
WHERE V.COMPANYIA='IBERIA'
AND P.NIF=B.NIF_PASSATGER
AND B.CODI_VOL=V.CODI_VOL
AND B.DATA=V.DATA
GROUP BY P.PAIS
ORDER BY 1, 2;


--4	CORRECTE(Aerolinies) Nom dels passatgers nascuts entre l’any 1960 i 1990.

SELECT DISTINCT P.NOM
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
AND P.DATA_NAIXEMENT >= TO_DATE('01/01/1960','DD/MM/YYYY')
AND P.DATA_NAIXEMENT < TO_DATE('01/01/1991','DD/MM/YYYY')
ORDER BY 1

--5	CORRECTE (Aerolinies) Nom del/s passatger/s que sempre vola en 2a classe.

SELECT DISTINCT P.NOM
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
AND B.CLASSE='2'
AND B.NIF_PASSATGER NOT IN (SELECT B.NIF_PASSATGER 
                            FROM BITLLET B, PERSONA P
                            WHERE P.NIF=B.NIF_PASSATGER
                            AND B.CLASSE='1')
    
ORDER BY 1;

--6	CORRECTE (Aerolinies) Nom del/s passatger/s que sempre vola en 2a classe.

SELECT DISTINCT P.NOM
FROM PERSONA P, BITLLET B
WHERE P.NIF=B.NIF_PASSATGER
AND B.CLASSE='2'
AND B.NIF_PASSATGER NOT IN (SELECT B.NIF_PASSATGER 
                            FROM BITLLET B, PERSONA P
                            WHERE P.NIF=B.NIF_PASSATGER
                            AND B.CLASSE='1')
    
ORDER BY 1;

--7	(Aerolinies) tipus d’avió amb més capacitat (quantitat de seients).

SELECT DISTINCT V.TIPUS_AVIO, MAX(S.FILA)
FROM VOL V, SEIENT S
WHERE V.CODI_VOL=S.CODI_VOL
AND V.DATA=S.DATA
GROUP BY V.TIPUS_AVIO
HAVING MAX(S.FILA) >= ALL (SELECT MAX(S.FILA)
                            FROM VOL V, SEIENT S
                            WHERE V.CODI_VOL=S.CODI_VOL
                            AND V.DATA=S.DATA
                            GROUP BY V.TIPUS_AVIO)
ORDER BY 1;


--8	(Aerolinies) Nom del(s) aeroport(s) amb el mínim de terminals possible.

SELECT DISTINCT A.NOM
FROM AEROPORT A , PORTA_EMBARCAMENT P
WHERE A.CODI_AEROPORT=P.CODI_AEROPORT
GROUP BY A.NOM, P.TERMINAL
HAVING COUNT(P.TERMINAL) <= ALL
                  ( SELECT COUNT(P.TERMINAL) 
                    FROM PORTA_EMBARCAMENT P, AEROPORT A 
                    WHERE A.CODI_AEROPORT=P.CODI_AEROPORT
                    GROUP BY A.NOM, P.TERMINAL
                  )
ORDER BY 1;



