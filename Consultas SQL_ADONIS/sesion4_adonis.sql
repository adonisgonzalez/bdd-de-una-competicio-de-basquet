--1)Nom de totes les persones que son del mateix país que Domingo Diaz Festivo (ell inclòs).
SELECT P.NOM, P.PAIS
FROM PERSONA P
WHERE P.PAIS like (select p.pais from persona p where p.nom like 'Domingo Diaz Festivo')
ORDER BY 1;


--2) ipus davio amb mes files.
SELECT DISTINCT TIPUS_AVIO 
FROM VOL V, SEIENT S
WHERE V.CODI_VOL=S.CODI_VOL AND V.DATA=S.DATA AND S.FILA=(SELECT MAX(FILA) FROM SEIENT)
ORDER BY 1;

--3) Nom del(s )  aero port( s)  amb més terminals
SELECT DISTINCT A.NOM
FROM AEROPORT A, PORTA_EMBARCAMENT P
WHERE P.TERMINAL = (SELECT MAX(TERMINAL) FROM PORTA_EMBARCAMENT)
AND A.CODI_AEROPORT=P.CODI_AEROPORT
ORDER BY 1;

--4) NIF,  nom i pes total facturat del passatger amb menys pes facturat en el    conjunt de tots els seus vols.  
SELECT P.NIF, P.NOM, SUM(M.PES) AS TOTAL
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY P.NIF, P.NOM
HAVING SUM(M.PES) <=ALL(SELECT SUM(M.PES)
                    FROM PERSONA P, BITLLET B, MALETA M
                    WHERE P.NIF=B.NIF_PASSATGER
                    AND B.NIF_PASSATGER=M.NIF_PASSATGER
                    AND B.CODI_VOL=M.CODI_VOL
                    AND B.DATA=M.DATA
                    GROUP BY P.NIF, P.NOM)
ORDER BY 1, 2, 3;

--5)Nombre de vegades que apareix la nacionalitat més freqüent entre  els passatgers de la BD. Atri buts: nacionalitat i nombre de vegades com a n_vegades
SELECT P.PAIS, COUNT(P.NOM) AS N_VEGADES
FROM PERSONA P
GROUP BY P.PAIS
HAVING COUNT(*) >= ALL (SELECT COUNT(P.NOM) from PERSONA P GROUP BY P.PAIS)
ORDER BY 1;

--6) NIF  i nom del(s) passatger(s) que ha(n ) facturatmés maletesen els seus vols. 
SELECT P.NIF, P.NOM, COUNT(CODI_MALETA) AS N_MALETES
FROM PERSONA P, BITLLET B, MALETA M
WHERE P.NIF=B.NIF_PASSATGER
AND B.NIF_PASSATGER=M.NIF_PASSATGER
AND B.CODI_VOL=M.CODI_VOL
AND B.DATA=M.DATA
GROUP BY P.NIF, P.NOM
HAVING COUNT(CODI_MALETA) >= ALL (SELECT COUNT(CODI_MALETA)
                            FROM PERSONA P, BITLLET B, MALETA M
                            WHERE P.NIF=B.NIF_PASSATGER
                            AND B.NIF_PASSATGER=M.NIF_PASSATGER
                            AND B.CODI_VOL=M.CODI_VOL
                            AND B.DATA=M.DATA
                            GROUP BY P.NIF, P.NOM)
ORDER BY 1, 2;


--7)NIF  i nom de la(/es) persona(/es) que ha(n) reservat més bitllets

DUDA (NO DA MISMO RESULTADO QUE EN SOLUCIÓN)
SELECT P.NIF, P.NOM, COUNT(R.LOCALITZADOR) AS TOTAL_RESERVES
FROM PERSONA P, RESERVA R
WHERE P.NIF=R.NIF_CLIENT
GROUP BY P.NIF, P.NOM
HAVING COUNT(R.LOCALITZADOR) >= ALL(SELECT COUNT(R.LOCALITZADOR)
                                FROM PERSONA P, RESERVA R
                                WHERE P.NIF=R.NIF_CLIENT
                                GROUP BY P.NIF, P.NOM)
ORDER BY 1, 2, 3;
--8)Nom de la(/es) companyia(/es) que té(nen) més bitllets reservats en algun dels seus vols. Atributs: companyia, codi i data de vol, n_seients_reservats
CASI 
SELECT DISTINCT V.COMPANYIA, V.CODI_VOL, V.DATA, S.FILA, S.LLETRA
FROM VOL V, SEIENT S
WHERE S.NIF_PASSATGER IN (SELECT S.NIF_PASSATGER FROM SEIENT S)
AND V.CODI_VOL=S.CODI_VOL
AND V.DATA=S.DATA
group by V.COMPANYIA, V.CODI_VOL, V.DATA, S.FILA, S.LLETRA
ORDER BY 3;


--9)Tipus d'avió(ns) que té(nen) més passatgers no espanyols. 

